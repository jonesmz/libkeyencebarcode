TEMPLATE = subdirs
SUBDIRS = library \
          dbus    \
          daemon

daemon.depends = library dbus
dbus.depends   = library
