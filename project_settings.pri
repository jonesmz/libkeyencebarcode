PROJ_DIR     = $$PWD

QT          -= gui
CONFIG      += c++11 separate_debug_info
VERSION      = 0.0.1

DESTDIR      = $$PROJ_DIR/dist

MOC_DIR      = .moc
UI_DIR       = .ui
RCC_DIR      = .rcc
OBJECTS_DIR  = .obj

INCLUDEPATH += $$PROJ_DIR/include include
