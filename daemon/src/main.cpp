#include <QtCore/QCoreApplication>

#include "KeyenceTypes"
#include "KeyenceRegisterTypes"
#include "BL_700"
#include "BL700Adaptor"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    KeyenceRegisterTypes::registerMetaTypes();

    BL_700 * portReader = new BL_700(QString(argv[1]));
    new Bl700Adaptor(portReader);

    QDBusConnection dbus = QDBusConnection::systemBus();

    dbus.registerObject("/BL_700", portReader);

    dbus.registerService("com.intechdirect.libkeyencebarcode.BL_700."+QString(argv[1]).replace(QChar('/'), QChar('-')));
    return a.exec();
}

