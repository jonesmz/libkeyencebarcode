#ifndef TYPES_H
#define TYPES_H

/*
 * Needed for Q_DECLARE_METATYPE
 */
#include <QMetaType>

/*
 * Needed to use QPair in the typedefs below.
 */
#include <QPair>

enum ModeOfOperation
{
      NormalMode   = 0x01
    , SettingsMode = 0x02
};

enum KeyenceError
{
      UndefinedCommand       = 0
    , CommandFormatIncorrect = 1
    , NothingCorresponds     = 2
    , WrongMValue            = 3
    , BarcodeTypeNotSent     = 4
    , NumberTooLong          = 5
    , DataTooShort           = 6
    , WrongNValue            = 7
    , InvalidNValue          = 8
    , InvalidNNNValue        = 9
    , NotInHex               = 10
    , FFCannotBeSet          = 11
    , TooManyCharacters      = 12
    , InvalidCharacters      = 13
    , EEPROMCorrupt          = 14
    , InitialSettingsError   = 15
    , SlashNotFollowed       = 16
    , TwoExclaimationPoints  = 17
    , Timeout                = 95
    , ParseError             = 96
    , WrongModeForFunction   = 97
    , NoError                = 98
    , ContactKEYENCE         = 99
};

enum Preset
{
      Code1 = 0
    , Code2 = 1
    , Code3 = 2
    , Code4 = 3
};

enum BarcodeType
{
      CODE39         = 0
    , ITF            = 1
    , Industrial2of5 = 2
    , Codabar        = 3
    , UPCEAN         = 4
    , CODE128        = 5
    , COOP2of5       = 6
    , None           = 7
    , CODE93         = 8
};

enum OututDigits
{
      ThirteenDigits = 0
    , TwelveDigits   = 1
};

enum ForwardReverse
{
      Forward = 0
    , Reverse = 1
};

enum CodeLengthDigits
{
      First       = 0
    , ThirtyFirst = 32
};

enum ReadingMode
{
      SingleLabel = 0
    , MultiLabel1 = 1
    , MultiLabel2 = 2
    , MultiLabel3 = 3
};

enum ReadDataTiming
{
      AfterReading  = 0
    , AfterInputOff = 1
};

enum DeviceBaudRate
{
      bps_9600  = 0
    , bps_4800  = 1
    , bps_2400  = 2
    , bps_1200  = 3
    , bps_600   = 4
    , bps_38400 = 5
    , bps_31250 = 6
    , bps_19200 = 7
};

enum DeviceParity
{
      device_even = 0
    , device_odd = 1
};

enum DeviceDataBits
{
      SevenBits = 0
    , EightBits = 1
};

enum DeviceStopBits
{
      OneBit = 0
    , TwoBits = 1
};

typedef QPair<KeyenceError, bool>             BoolReturn;
typedef QPair<KeyenceError, uchar>            UCharReturn;
typedef QPair<KeyenceError, QString>          StringReturn;
typedef QPair<KeyenceError, ReadingMode>      ReadingModeReturn;
typedef QPair<KeyenceError, BarcodeType>      BarcodeTypeReturn;
typedef QPair<KeyenceError, OututDigits>      OututDigitsReturn;
typedef QPair<KeyenceError, DeviceParity>     DeviceParityReturn;
typedef QPair<KeyenceError, DeviceDataBits>   DeviceDataBitsReturn;
typedef QPair<KeyenceError, DeviceStopBits>   DeviceStopBitsReturn;
typedef QPair<KeyenceError, ReadDataTiming>   ReadDataTimingReturn;
typedef QPair<KeyenceError, ForwardReverse>   ForwardReverseReturn;
typedef QPair<KeyenceError, DeviceBaudRate>   DeviceBaudRateReturn;
typedef QPair<KeyenceError, CodeLengthDigits> CodeLengthDigitsReturn;


Q_DECLARE_METATYPE(KeyenceError)
Q_DECLARE_METATYPE(Preset)
Q_DECLARE_METATYPE(DeviceBaudRate)
Q_DECLARE_METATYPE(OututDigits)
Q_DECLARE_METATYPE(BarcodeType)
Q_DECLARE_METATYPE(ReadingMode)
Q_DECLARE_METATYPE(ReadDataTiming)
Q_DECLARE_METATYPE(ForwardReverse)
Q_DECLARE_METATYPE(ModeOfOperation)
Q_DECLARE_METATYPE(CodeLengthDigits)

Q_DECLARE_METATYPE(BoolReturn)
Q_DECLARE_METATYPE(UCharReturn)
Q_DECLARE_METATYPE(StringReturn)
Q_DECLARE_METATYPE(ReadingModeReturn)
Q_DECLARE_METATYPE(BarcodeTypeReturn)
Q_DECLARE_METATYPE(OututDigitsReturn)
Q_DECLARE_METATYPE(DeviceParityReturn)
Q_DECLARE_METATYPE(DeviceDataBitsReturn)
Q_DECLARE_METATYPE(DeviceStopBitsReturn)
Q_DECLARE_METATYPE(ReadDataTimingReturn)
Q_DECLARE_METATYPE(ForwardReverseReturn)
Q_DECLARE_METATYPE(DeviceBaudRateReturn)
Q_DECLARE_METATYPE(CodeLengthDigitsReturn)

#endif //TYPES_H
