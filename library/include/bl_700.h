/*
 * This file is part of libkeyencebarcode.
 *
 * libkeyencebarcode is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkeyencebarcode is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkeyencebarcode. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2013 IntechDirect, Inc.
 */

#ifndef BL_700_H
#define BL_700_H

#include <QMutex>
#include <QObject>

#include "keyencetypes.h"

class QSerialPort;

class BL_700 : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.intechdirect.libkeyence.bl700")

public:
    enum KeyenceParameterCodes
    {
          CODE39SendingStartStopCharacter = 0
        , CODE39InspectionOfCheckDigit = 1
        , CODE39SendingCheckDigit = 2
        , ITFInspectionOfCheckDigit = 10
        , ITFSendingCheckDigit = 11
        , CodabarSendingStartStopCharacter = 30
        , CodabarStopStartCharacterType = 31
        , CodabarInspectionOfCheckDigit = 32
        , CodabarSendingCheckDigit = 33
        , CodabarSettingCheckDigitType = 34
        , EANUPCReadingUPC_E = 40
        , EANUPCReadingEAN8Digits = 41
        , EANUPCReadingJAN13Digits = 42
        , EANUPCNoOfUPC_AOutputDigits = 43
        , EANAdding0ToUPC_ESystemCode = 44
        , CODE128CheckingDoubleCharacterStartPattern = 51
        , MaxCodeLengthOutputFunction = 83
        , DirectionForMaxCodeLengthOutput = 84
        , EffectiveDigitsForMaxCodeLengthOutput = 85
        , StartDigitForMaxCodeLengthOutput = 86
        , LabelOrientationSpecifiedReading = 81
        , OrientationForOrientationSpecifiedReading = 82
        , ReadingModeCmd = 12
        , DataSendTiming = 13
        , RepeatReadTimeInMultiLabelReadingMode1Or2 = 41
        , SetDecodingMatchCount = 43
        , DecodingMatchCountInAddnlInfo = 10
        , ScansInAddnlInfo = 11
        , LabelOrientationInAddnlInfo = 14
        , CodeTypeInAddnlInfo = 17
        , ReadingKeyenceErrorCode =  55
        , SignalType = 5
        , OneShotInputTime = 42
        , TimeConstantOfTrigger = 4
        , StateOfTriggerInput = 3
        , StartTestModeWhenTriggerInputTurnsOn = 6
        , CharactersOfTriggerOnCommand = 56
        , CharactersOfTriggerOffCommand = 57
        , DeviceBaudRateCmd = 35
        , DataBitLength = 30
        , ParityCheck = 31
        , ParityType = 32
        , StopBit = 33
        , RTS_CTSHandshaking = 22
        , RS_485MultiDropLink = 34
        , IDForRS_485MultiDropLink = 44
        , HandshakeProtocol1 = 7
        , HandshakeProtocol2 = 8
        , AddingChecksum = 39
        , Header = 51
        , Delimiter = 52
        , PLCLinkModel = 36
        , TriggerInputThroughPLCLink = 37
        , DMHeadAddress = 45
        , PLCStationNumber = 47
        , FileRegisterBlockNumber = 46
        , PartitionMarkWhenAdditionalInformationIsUsed = 50
        , IntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed = 54
        , IndicationOfStabilityLED = 9
        , OK_NGOutputDuration = 40
        , RegistrationOfPresetDataForCompare = 68
    };

public Q_SLOTS:
    /*!
     * \brief setBarCodeType
     * \param [in] code
     * \param [in] type
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  CODEm=n OK
     *
     * n=0: CODE 39
     *    1: ITF
     *    2: Industrial2of5
     *    3: Codabar
     *    4: UPC/EAN
     *    5: CODE128
     *    6: COOP2of5
     *    7: None
     *    8: CODE93
     *
     * Setting bar code type for codes 1 to 4.
     */
   KeyenceError setBarCodeType(Preset code, BarcodeType type);

    /*!
     * \brief getBarCodeType
     * \param [in] code
     * \param [out] type
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm CODEm   mn
     *
     * n=0: CODE 39
     *    1: ITF
     *    2: Industrial2of5
     *    3: Codabar
     *    4: UPC/EAN
     *    5: CODE128
     *    6: COOP2of5
     *    7: None
     *    8: CODE93
     *
     * Reading bar code type for codes 1 to 4.
     */
   BarcodeTypeReturn getBarCodeType(Preset code);


    /*!
     * \brief setMaxNumberOfReadableDigits
     * \param [in] code
     * \param [in] digits
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  MAXm=nn OK
     *
     * m=0 to 3: Codes 1 to 4
     * nn=01 to 32
     * For CODE39, Codabar: 03 to 32
     * For ITF: 02 to 32
     * For CODE128: 01 to 64
     * Note: With EAN code, this command causesKeyenceError.
     *
     * Setting Max. No. of readable digits.
     */
   KeyenceError setMaxNumberOfReadableDigits(Preset code, uchar digits);

    /*!
     * \brief getMaxNumberOfReadableDigits
     * \param [in] code
     * \param [out] digits
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm MAXm    mnn
     *
     * m=0 to 3: Codes 1 to 4
     * nn=01 to 32
     * For CODE39, Codabar: 03 to 32
     * For ITF: 02 to 32
     * For CODE128: 01 to 64
     * Note: With EAN code, this command causesKeyenceError.
     *
     * Reading Max. No. of readable digits.
     */
   QPair<KeyenceError, uchar> getMaxNumberOfReadableDigits(Preset code);

    /*!
     * \brief setMinNumberOfReadableDigits
     * \param [in] code
     * \param [in] digits
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  MINm=nn OK
     *
     * m=0 to 3: Codes 1 to 4
     * nn=01 to 32
     * For CODE39, Codabar: 03 to 32
     * For ITF: 02 to 32
     * For CODE128: 01 to 64
     * Note: With EAN code, this command causesKeyenceError.
     *
     * Setting Max. No. of readable digits.
     */
   KeyenceError setMinNumberOfReadableDigits(Preset code, uchar digits);

    /*!
     * \brief getMinNumberOfReadableDigits
     * \param [in] code
     * \param [out] digits
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm MINm    mnn
     *
     * m=0 to 3: Codes 1 to 4
     * nn=01 to 32
     * For CODE39, Codabar: 03 to 32
     * For ITF: 02 to 32
     * For CODE128: 01 to 64
     * Note: With EAN code, this command causesKeyenceError.
     *
     * Setting Max. No. of readable digits.
     */
   UCharReturn getMinNumberOfReadableDigits(Preset code);

    /*!
     * \brief setCODE39SendingStartStopCharacter
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm00n OK
     *
     * CODE39 Sending start/stop character.
     * m=0 to 3: Codes 1 to 4
     * n=0: Disable
     *   1: Enable
     *
     * CODE39 Sending start/stop character.
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setCODE39SendingStartStopCharacter(Preset code, bool enable);

    /*!
     * \brief getCODE39SendingStartStopCharacter
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm00  00n
     *
     * CODE39 Sending start/stop character.
     * m=0 to 3: Codes 1 to 4
     * n=0: Disable
     *   1: Enable
     *
     * CODE39 Sending start/stop character.
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getCODE39SendingStartStopCharacter(Preset code);

    /*!
     * \brief setCODE39InspectionOfCheckDigit
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm01n OK
     *
     * CODE39 Inspection of check digit (Modulus 43)
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setCODE39InspectionOfCheckDigit(Preset code, bool setting);

    /*!
     * \brief getCODE39InspectionOfCheckDigit
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm01  01n
     *
     * CODE39 Inspection of check digit (Modulus 43)
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getCODE39InspectionOfCheckDigit(Preset code);

    /*!
     * \brief setCODE39SendingCheckDigit
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm02n OK
     *
     * CODE39 Sending check digit.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setCODE39SendingCheckDigit(Preset code, bool setting);

    /*!
     * \brief getCODE39SendingCheckDigit
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm02  02n
     *
     * CODE39 Sending check digit.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getCODE39SendingCheckDigit(Preset code);

    /*!
     * \brief setITFInspectionOfCheckDigit
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm10n OK
     *
     * ITF Inspection of check digit (Modulus 10/Wait 3)
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setITFInspectionOfCheckDigit(Preset code, bool setting);

    /*!
     * \brief getITFInspectionOfCheckDigit
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm10  10n
     *
     * ITF Inspection of check digit (Modulus 10/Wait 3)
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getITFInspectionOfCheckDigit(Preset code);


    /*!
     * \brief setITFSendingCheckDigit
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm11n OK
     *
     * ITF Sending check digit.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setITFSendingCheckDigit(Preset code, bool setting);

    /*!
     * \brief getITFSendingCheckDigit
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm11  11n
     *
     * ITF Sending check digit.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getITFSendingCheckDigit(Preset code);

    /*!
     * \brief setCodabarSendingStartStopCharacter
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm30n OK
     *
     * Codabar Sending start/stop character.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setCodabarSendingStartStopCharacter(Preset code, bool setting);

    /*!
     * \brief getCodabarSendingStartStopCharacter
     * \param [out] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm30  30n
     *
     * Codabar Sending start/stop character.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getCodabarSendingStartStopCharacter(Preset code);

    /*!
     * \brief setCodabarStopStartCharacterType
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm31n OK
     *
     * Codabar Start/Stop character type.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setCodabarStopStartCharacterType(Preset code, bool setting);

    /*!
     * \brief getCodabarStopStartCharacterType
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm31  31n
     *
     * Codabar Start/Stop character type.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getCodabarStopStartCharacterType(Preset code);

    /*!
     * \brief setCodabarInspectionOfCheckDigit
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm32n OK
     *
     * Codabar Inspection of check digit.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setCodabarInspectionOfCheckDigit(Preset code, bool setting);

    /*!
     * \brief getCodabarInspectionOfCheckDigit
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm32  32n
     *
     * Codabar Inspection of check digit.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getCodabarInspectionOfCheckDigit(Preset code);

    /*!
     * \brief setCodabarSendingCheckDigit
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm33n OK
     *
     * Codabar Sending check digit.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setCodabarSendingCheckDigit(Preset code, bool setting);

    /*!
     * \brief getCodabarSendingCheckDigit
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm33  33n
     *
     * Codabar Sending check digit.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getCodabarSendingCheckDigit(Preset code);

    /*!
     * \brief setCodabarSettingCheckDigitType
     * \param [in] code
     * \param [in] checktype
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm34n OK
     *
     * Codabar Setting check digit type.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 08, 14
     */
   KeyenceError setCodabarSettingCheckDigitType(Preset code, BarcodeType checktype);

    /*!
     * \brief getCodabarSettingCheckDigitType
     * \param [in] code
     * \param [out] checktype
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm34  34n
     *
     * Codabar Setting check digit type.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 08, 14
     */
   BarcodeTypeReturn getCodabarSettingCheckDigitType(Preset code);


    /*!
     * \brief setEANUPCReadingUPC_E
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm40n OK
     *
     * EAN/UPC (A•E) Reading UPC-E
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setEANUPCReadingUPC_E(Preset code, bool setting);

    /*!
     * \brief getEANUPCReadingUPC_E
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm40  40n
     *
     * EAN/UPC (A•E) Reading UPC-E
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getEANUPCReadingUPC_E(Preset code);

    /*!
     * \brief setEANUPCReadingEAN8Digits
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * EAN/UPC (A•E) Reading EAN 8 digits
     *
     * Change  WCm41n OK
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setEANUPCReadingEAN8Digits(Preset code, bool setting);

    /*!
     * \brief setEANUPCReadingEAN8Digits
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * EAN/UPC (A•E) Reading EAN 8 digits
     *
     * Confirm RCm41  41n
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getEANUPCReadingEAN8Digits(Preset code);

    /*!
     * \brief setEANUPCReadingJAN13Digits
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * EAN/UPC(A•E) Reading JAN 13 digits
     *
     * Change  WCm42n OK
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setEANUPCReadingJAN13Digits(Preset code, bool setting);

    /*!
     * \brief getEANUPCReadingJAN13Digits
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * EAN/UPC(A•E) Reading JAN 13 digits
     *
     * Confirm RCm42  42n
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getEANUPCReadingJAN13Digits(Preset code);

    /*!
     * \brief setEANUPCNoOfUPC_AOutputDigits
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm43n OK
     *
     * EAN/UPC(A•E) No. of UPC-A output digits.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setEANUPCNoOfUPC_AOutputDigits(Preset code, OututDigits setting);

    /*!
     * \brief getEANUPCNoOfUPC_AOutputDigits
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm43  43n
     *
     * EAN/UPC(A•E) No. of UPC-A output digits.
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   OututDigitsReturn getEANUPCNoOfUPC_AOutputDigits(Preset code);

    /*!
     * \brief setEANAdding0ToUPC_ESystemCode
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm44n OK
     *
     * EAN/UPC(A•E) Adding “0” to UPC-E system code
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setEANAdding0ToUPC_ESystemCode(Preset code, bool setting);

    /*!
     * \brief getEANAdding0ToUPC_ESystemCode
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm44  44n
     *
     * EAN/UPC(A•E) Adding “0” to UPC-E system code
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getEANAdding0ToUPC_ESystemCode(Preset code);

    /*!
     * \brief setCODE128CheckingDoubleCharacterStartPattern
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm51n OK
     *
     * CODE128 Checking double character start pattern
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setCODE128CheckingDoubleCharacterStartPattern(Preset code, bool setting);

    /*!
     * \brief getCODE128CheckingDoubleCharacterStartPattern
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm51  51n
     *
     * CODE128 Checking double character start pattern
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getCODE128CheckingDoubleCharacterStartPattern(Preset code);

    /*!
     * \brief setMaxCodeLengthOutputFunction
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm83n OK
     *
     * Setting max. code length output function
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setMaxCodeLengthOutputFunction(Preset code, bool setting);

    /*!
     * \brief getMaxCodeLengthOutputFunction
     * \param [out] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm83 83n
     *
     * Setting max. code length output function
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getMaxCodeLengthOutputFunction(Preset code);

    /*!
     * \brief setDirectionForMaxCodeLengthOutput
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm84n OK
     *
     * Setting direction for max. code length output
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setDirectionForMaxCodeLengthOutput(Preset code, ForwardReverse setting);

    /*!
     * \brief getDirectionForMaxCodeLengthOutput
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm84  84n
     *
     * Setting direction for max. code length output
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   ForwardReverseReturn getDirectionForMaxCodeLengthOutput(Preset code);

    /*!
     * \brief setEffectiveDigitsForMaxCodeLengthOutput
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm85n OK
     * Confirm RCm85  85n
     *
     * Setting effective digits for max. code length output
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setEffectiveDigitsForMaxCodeLengthOutput(Preset code, CodeLengthDigits setting);

    /*!
     * \brief getEffectiveDigitsForMaxCodeLengthOutput
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm85  85n
     *
     * Setting effective digits for max. code length output
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   CodeLengthDigitsReturn getEffectiveDigitsForMaxCodeLengthOutput(Preset code);

    /*!
     * \brief setStartDigitForMaxCodeLengthOutput
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm86n OK
     *
     * Setting starting digit for max. code length output
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setStartDigitForMaxCodeLengthOutput(Preset code, CodeLengthDigits setting);

    /*!
     * \brief setStartDigitForMaxCodeLengthOutput
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm86  86n
     *
     * Setting starting digit for max. code length output
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   CodeLengthDigitsReturn getStartDigitForMaxCodeLengthOutput(Preset code);

    /*!
     * \brief setLabelOrientationSpecifiedReading
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm81n OK
     *
     * Setting label orientation specified reading
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setLabelOrientationSpecifiedReading(Preset code, bool setting);

    /*!
     * \brief getLabelOrientationSpecifiedReading
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm81  81n
     *
     * Setting label orientation specified reading
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   BoolReturn getLabelOrientationSpecifiedReading(Preset code);

    /*!
     * \brief setOrientationForOrientationSpecifiedReading
     * \param [in] code
     * \param [in] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WCm82n OK
     *
     * Setting orientation for orientation-specified reading
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   KeyenceError setOrientationForOrientationSpecifiedReading(Preset code, ForwardReverse setting);

    /*!
     * \brief getOrientationForOrientationSpecifiedReading
     * \param [in] code
     * \param [out] setting
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RCm82  82n
     *
     * Setting orientation for orientation-specified reading
     *
     *KeyenceError Codes: 00, 02, 03, 04, 05, 07, 14
     */
   ForwardReverseReturn getOrientationForOrientationSpecifiedReading(Preset code);



    /*!
     * \brief setReadingMode
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP12n OK
     *
     * n=0: Single label
     *   1: Multi label 1
     *   2: Multi label 2
     *   3: Multi label 3
     *
     * Setting reading mode
     *
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   KeyenceError setReadingMode(ReadingMode mode);

    /*!
     * \brief getReadingMode
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP12  12n
     *
     * n=0: Single label
     *   1: Multi label 1
     *   2: Multi label 2
     *   3: Multi label 3
     *
     * Setting reading mode
     *
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   ReadingModeReturn getReadingMode();


    /*!
     * \brief setDataSendTiming
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP13n OK
     *
     *  N=0: Sends data after reading
     *    1: Sends after timing input turns off
     *
     * Setting data send timing
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setDataSendTiming(ReadDataTiming mode);

    /*!
     * \brief getDataSendTiming
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP13 13n
     *
     *  N=0: Sends data after reading
     *    1: Sends after timing input turns off
     *
     * Setting data send timing
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadDataTimingReturn  getDataSendTiming();

    /*!
     * \brief setRepeatReadTimeInMultiLabelReadingMode1Or2
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP41nnn OK
     *
     *  nnn=001 to 225 (by 100 ms step)
     *
     * Setting repeat-read
     * time in multi label
     * reading mode 1 or 2
     *
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setRepeatReadTimeInMultiLabelReadingMode1Or2(ReadingMode mode);

    /*!
     * \brief getRepeatReadTimeInMultiLabelReadingMode1Or2
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP41    41nnn
     *
     *  nnn=001 to 225 (by 100 ms step)
     *
     * Setting repeat-read
     * time in multi label
     * reading mode 1 or 2
     *
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   ReadingModeReturn  getRepeatReadTimeInMultiLabelReadingMode1Or2();

    /*!
     * \brief setSetDecodingMatchCount
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP43nnn OK
     *
     *  nnn=001 to 225
     *
     * Setting decoding match count
     *
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setSetDecodingMatchCount(ReadingMode mode);

    /*!
     * \brief getSetDecodingMatchCount
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP43    43nnn
     *
     *  nnn=001 to 225
     *
     * Setting decoding match count
     *
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   ReadingModeReturn  getSetDecodingMatchCount();


    /*!
     * \brief setDecodingMatchCountInAddnlInfo
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP10n OK
     *
     * n=0: No addition
     *   1: Add
     *
     * Setting decoding match count in additional information
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setDecodingMatchCountInAddnlInfo(ReadingMode mode);

    /*!
     * \brief setDecodingMatchCountInAddnlInfo
     * \param [out]  mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP10 10n
     *
     * n=0: No addition
     *   1: Add
     *
     * Setting decoding match count in additional information
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn  getDecodingMatchCountInAddnlInfo();

    /*!
     * \brief setScansInAddnlInfo
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP11n OK
     *
     *  n=0: No addition
     *    1: Add
     *    Note:  Effective only when No. of decodings are added.
     *
     * Setting scans in additional information
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setScansInAddnlInfo(ReadingMode mode);

    /*!
     * \brief getScansInAddnlInfo
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP11  11n
     *
     *  n=0: No addition
     *    1: Add
     *    Note:  Effective only when No. of decodings are added.
     *
     * Setting scans in additional information
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn  getScansInAddnlInfo();

    /*!
     * \brief setLabelOrientationInAddnlInfo
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP14n OK
     *
     *  n=0: No addition
     *    1: Add
     *
     * Setting label orientation in additional information
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setLabelOrientationInAddnlInfo(ReadingMode mode);

    /*!
     * \brief getLabelOrientationInAddnlInfo
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP14 14n
     *
     *  n=0: No addition
     *    1: Add
     *
     * Setting label orientation in additional information
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn  getLabelOrientationInAddnlInfo();

    /*!
     * \brief setCodeTypeInAddnlInfo
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP17n OK
     *
     *  n=0: No addition
     *    1: Add
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setCodeTypeInAddnlInfo(ReadingMode mode);

    /*!
     * \brief getCodeTypeInAddnlInfo
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP17  17n
     *
     *  n=0: No addition
     *    1: Add
     *
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn  getCodeTypeInAddnlInfo();

    /*!
     * \brief setReadingKeyenceErrorCode
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP55hhh... OK
     *
     *  hhh ... = ReadingKeyenceError code (within 8 characters)
     *  * Specify the characters in HEX (hexadecimal) code.
     *  * If the readingKeyenceError code is not specified, hhh... = FF.
     *
     *
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   KeyenceError setReadingKeyenceErrorCode(ReadingMode mode);

    /*!
     * \brief getReadingKeyenceErrorCode
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP55       55hhh...
     *
     *  hhh ... = ReadingKeyenceError code (within 8 characters)
     *  * Specify the characters in HEX (hexadecimal) code.
     *  * If the readingKeyenceError code is not specified, hhh... = FF.
     *
     *
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   ReadingModeReturn  getReadingKeyenceErrorCode();

    /*!
     * \brief setSignalType
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP05n OK
     *
     *  n=0: Level
     *    1: One-shot
     * Setting signal type
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setSignalType(ReadingMode mode);

    /*!
     * \brief setSignalType
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP05  05n
     *
     *  n=0: Level
     *    1: One-shot
     * Setting signal type
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn  getSignalType();

    /*!
     * \brief setOneShotInputTime
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP42nnn OK
     *
     *  nnn=001 to 225(by 100 ms step)
     *
     * Setting one-shot input time
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setOneShotInputTime(ReadingMode mode);

    /*!
     * \brief getOneShotInputTime
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP42 42nnn
     *
     *  nnn=001 to 225(by 100 ms step)
     *
     * Setting one-shot input time
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   ReadingModeReturn  getOneShotInputTime();

    /*!
     * \brief setTimeConstantOfTrigger
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP04n OK
     *
     *  n=0: 2 ms
     *    1: 10 ms
     *
     * Setting time constant of trigger input
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setTimeConstantOfTrigger(ReadingMode mode);

    /*!
     * \brief getTimeConstantOfTrigger
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP04 04n
     *
     *  n=0: 2 ms
     *    1: 10 ms
     *
     * Setting time constant of trigger input
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getTimeConstantOfTrigger();

    /*!
     * \brief setStateOfTriggerInput
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP06n OK
     *
     *  n=0: Normal-open
     *    1: Normal-close
     * Starting test mode when trigger input turns on.
     * (To specify the test mode, use the command below.)
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setStateOfTriggerInput(ReadingMode mode);

    /*!
     * \brief getStateOfTriggerInput
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP06 06n
     *
     *  n=0: Normal-open
     *    1: Normal-close
     * Starting test mode when trigger input turns on.
     * (To specify the test mode, use the command below.)
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn  getStateOfTriggerInput();

    /*!
     * \brief setStartTestModeWhenTriggerInputTurnsOn
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP06n OK
     *
     *   n=0: Disable
     *     1: Enable
     *
     * Specifying the test mode to be started when trigger input turns on.
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   KeyenceError setStartTestModeWhenTriggerInputTurnsOn(ReadingMode mode);

    /*!
     * \brief getStartTestModeWhenTriggerInputTurnsOn
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP06 06n
     *
     *   n=0: Disable
     *     1: Enable
     *
     * Specifying the test mode to be started when trigger input turns on.
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   ReadingModeReturn  getStartTestModeWhenTriggerInputTurnsOn();

    /*!
     * \brief setTestModeToStartWhenTriggerInputTurnsOn
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change TRGTn OK
     *
     *   N=1: Reading rate check mode
     *     2: Tact check mode
     * Specifying the test mode to be started when trigger input turns on.
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   KeyenceError setTestModeToStartWhenTriggerInputTurnsOn(ReadingMode mode);

    /*!
     * \brief getTestModeToStartWhenTriggerInputTurnsOn
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm TRGT TRGTn
     *
     *   N=1: Reading rate check mode
     *     2: Tact check mode
     * Specifying the test mode to be started when trigger input turns on.
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   ReadingModeReturn  getTestModeToStartWhenTriggerInputTurnsOn();

    /*!
     * \brief setStartTestModeWhenPowerOn
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change TESTn OK
     *
     *    n=0: Reset
     *      1: Reading rate check mode
     *      2: Tact check mode
     * Starting test mode when power is turned on.
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   KeyenceError setStartTestModeWhenPowerOn(ReadingMode mode);
    /*!
     * \brief setStartTestModeWhenPowerOn
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm TEST TESTn
     *
     *    n=0: Reset
     *      1: Reading rate check mode
     *      2: Tact check mode
     * Starting test mode when power is turned on.
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   ReadingModeReturn  getStartTestModeWhenPowerOn();

    /*!
     * \brief setCharactersOfTriggerOnCommand
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP56hhh... OK
     * Confirm RP56 56hhh...
     *
     *  hhh...=Trigger on command (Up to 8 characters)
     *  * Specify characters in HEX (hexadecimal) code.
     * Setting characters of trigger on command
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   KeyenceError setCharactersOfTriggerOnCommand(ReadingMode mode);

    /*!
     * \brief getCharactersOfTriggerOnCommand
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP56 56hhh...
     *
     *  hhh...=Trigger on command (Up to 8 characters)
     *  * Specify characters in HEX (hexadecimal) code.
     * Setting characters of trigger on command
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   ReadingModeReturn getCharactersOfTriggerOnCommand();

    /*!
     * \brief setCharactersOfTriggerOffCommand
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP57hhh... OK
     *
     *  hhh...=Trigger on command (Up to 8 characters)
     *  * Specify characters in HEX (hexadecimal) code.
     * Setting characters of trigger on command
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   KeyenceError setCharactersOfTriggerOffCommand(ReadingMode mode);

    /*!
     * \brief getCharactersOfTriggerOffCommand
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP57 57hhh...
     *
     *  hhh...=Trigger on command (Up to 8 characters)
     *  * Specify characters in HEX (hexadecimal) code.
     * Setting characters of trigger on command
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   ReadingModeReturn getCharactersOfTriggerOffCommand();


    /*!
     * \brief setDeviceBaudRate
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP35n OK
     *
     *  N=5: 38400 bps
     *    6: 31250 bps
     *    7: 19200 bps
     *    0: 9600 bps
     *    1: 4800 bps
     *    2: 2400 bps
     *    3: 1200 bps
     *    4: 600 bps
     * Setting baud rate
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   KeyenceError setDeviceBaudRate(DeviceBaudRate rate);

    /*!
     * \brief getDeviceBaudRate
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP35 035n
     *
     *  N=5: 38400 bps
     *    6: 31250 bps
     *    7: 19200 bps
     *    0: 9600 bps
     *    1: 4800 bps
     *    2: 2400 bps
     *    3: 1200 bps
     *    4: 600 bps
     * Setting baud rate
     *KeyenceError Codes: 00, 02, 05, 08, 14
     */
   DeviceBaudRateReturn getDeviceBaudRate();

    /*!
     * \brief setDataBitLength
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP30n OK
     *
     *  n=0: 7 bits
     *    1: 8 bits
     * Setting data bit length
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setDataBitLength(DeviceDataBits mode);

    /*!
     * \brief getDataBitLength
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP30 30n
     *
     *  n=0: 7 bits
     *    1: 8 bits
     * Setting data bit length
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   DeviceDataBitsReturn getDataBitLength();

    /*!
     * \brief setParityCheck
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP31n OK
     *
     * n=0: Disable
     *   1: Enable
     *
     *  Setting parity check
     *  * To set the parity type, use the command below.
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setParityCheck(bool mode);

    /*!
     * \brief getParityCheck
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP31  31n
     *
     * n=0: Disable
     *   1: Enable
     *
     *  Setting parity check
     *  * To set the parity type, use the command below.
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   BoolReturn getParityCheck();


    /*!
     * \brief setParityType
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP32n OK
     *
     * n=0: Even
     *   1: Odd
     *
     *  Setting parity check
     *  * To set the parity type, use the command below.
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setParityType(DeviceParity mode);

    /*!
     * \brief getParityType
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP32 32n
     *
     * n=0: Even
     *   1: Odd
     *
     *  Setting parity check
     *  * To set the parity type, use the command below.
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   DeviceParityReturn getParityType();

    /*!
     * \brief setStopBit
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP33n OK
     *
     * n=0: 1 bit
     *   1: 2 bits
     *
     *  Setting Stop bit
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setStopBit(DeviceStopBits mode);

    /*!
     * \brief getStopBit
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP33 33n
     *
     * n=0: 1 bit
     *   1: 2 bits
     *
     *  Setting Stop bit
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   DeviceStopBitsReturn getStopBit();


    /*!
     * \brief setRTS_CTSHandshaking
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP22n OK
     *
     * n=0: Disable
     *   1: Enable
     *
     *  Setting RTS/CTS handshaking
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setRTS_CTSHandshaking(ReadingMode mode);


    /*!
     * \brief getRTS_CTSHandshaking
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP22 22n
     *
     * n=0: Disable
     *   1: Enable
     *
     *  Setting RTS/CTS handshaking
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getRTS_CTSHandshaking();


    /*!
     * \brief setRS_485MultiDropLink
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP34n OK
     *
     * n=0: Disable
     *   1: Enable
     *
     *  Setting RS-485 multi drop link
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setRS_485MultiDropLink(ReadingMode mode);

    /*!
     * \brief getRS_485MultiDropLink
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP34 34n
     *
     * n=0: Disable
     *   1: Enable
     *
     *  Setting RS-485 multi drop link
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getRS_485MultiDropLink();

    /*!
     * \brief setIDForRS_485MultiDropLink
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP44nn OK
     *
     * nn=01 to 31
     *
     *  Setting RTS/CTS handshaking
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setIDForRS_485MultiDropLink(ReadingMode mode);

    /*!
     * \brief setIDForRS_485MultiDropLink
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP44 44nn
     *
     * nn=01 to 31
     *
     *  Setting RTS/CTS handshaking
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   ReadingModeReturn getIDForRS_485MultiDropLink();

    /*!
     * \brief setHandshakeProtocol1
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP07n OK
     *
     * n = 0: No handshaking
     * n = 1: Use protocol
     * * To set details of the protocol, use handshaking protocol 2.
     *
     *  Handshaking protocol 1
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setHandshakeProtocol1(ReadingMode mode);

    /*!
     * \brief getHandshakeProtocol1
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP07 07n
     *
     * n = 0: No handshaking
     * n = 1: Use protocol
     * * To set details of the protocol, use handshaking protocol 2.
     *
     *  Handshaking protocol 1
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getHandshakeProtocol1();

    /*!
     * \brief setHandshakeProtocol2
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP08n OK
     *
     * n = 0: PASS/RTRY protocol
     * n = 1: ACK/NAK protocol
     *
     *  Handshaking protocol 1
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setHandshakeProtocol2(ReadingMode mode);

    /*!
     * \brief setHandshakeProtocol2
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP08 08n
     *
     * n = 0: PASS/RTRY protocol
     * n = 1: ACK/NAK protocol
     *
     *  Handshaking protocol 1
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getHandshakeProtocol2();

    /*!
     * \brief setAddingChecksum
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP39n OK
     *
     * n = 0: Do not add
     * n = 1: Add
     *
     *  Adding checksum
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setAddingChecksum(ReadingMode mode);

    /*!
     * \brief setAddingChecksum
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP39 39n
     *
     * n = 0: Do not add
     * n = 1: Add
     *
     *  Adding checksum
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getAddingChecksum();


    /*!
     * \brief setHeader
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP51hhh...  OK
     *
     *  hhh... = Header (up to five characters)
     *   * To set a header, use HEX (hexadecimal) codes.
     *   * hhh... = FF: Header is not set.
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   KeyenceError setHeader(ReadingMode mode);

    /*!
     * \brief setHeader
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP51 51hhh...
     *
     *  hhh... = Header (up to five characters)
     *   * To set a header, use HEX (hexadecimal) codes.
     *   * hhh... = FF: Header is not set.
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   ReadingModeReturn getHeader();


    /*!
     * \brief setDelimiter
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP52hhh... OK
     *
     * hhh... = Delimiter (up to five characters)
     * * To set a delimiter, use HEX (hexadecimal) codes.
     * * hhh... = FF: Delimiter is not set.
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   KeyenceError setDelimiter(ReadingMode mode);

    /*!
     * \brief getDelimiter
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP52 52hhh...
     *
     * hhh... = Delimiter (up to five characters)
     * * To set a delimiter, use HEX (hexadecimal) codes.
     * * hhh... = FF: Delimiter is not set.
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   ReadingModeReturn getDelimiter();

    /*!
     * \brief setPLCLinkModel
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP36n OK
     *
     * n = 0: PLC link disabled
     * n = 1: SYSNAC-C
     * n = 2: MELSEC-A
     * n = 3: KV
     * n = 4: MELSEC-A (File register is used.)
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setPLCLinkModel(ReadingMode mode);

    /*!
     * \brief getPLCLinkModel
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP36 36n
     *
     * n = 0: PLC link disabled
     * n = 1: SYSNAC-C
     * n = 2: MELSEC-A
     * n = 3: KV
     * n = 4: MELSEC-A (File register is used.)
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getPLCLinkModel();


    /*!
     * \brief setTriggerInputThroughPLCLink
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP37n  OK
     *
     * n = 0: Disable
     * n = 1: Enable
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setTriggerInputThroughPLCLink(ReadingMode mode);

    /*!
     * \brief getTriggerInputThroughPLCLink
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP37 37n
     *
     * n = 0: Disable
     * n = 1: Enable
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getTriggerInputThroughPLCLink();

    /*!
     * \brief setDMHeadAddress
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change WP45nn   OK
     * Confirm RP45  45nn
     *
     * nn = 00 to 99 (0000 to 9900)
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setDMHeadAddress(ReadingMode mode);

    /*!
     * \brief getDMHeadAddress
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP45  45nn
     *
     * nn = 00 to 99 (0000 to 9900)
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   ReadingModeReturn getDMHeadAddress();


    /*!
     * \brief setPLCStationNumber
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP47nn OK
     *
     * nn = 00 to 31
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setPLCStationNumber(ReadingMode mode);

    /*!
     * \brief getPLCStationNumber
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP47   47nn
     *
     * nn = 00 to 31
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   ReadingModeReturn getPLCStationNumber();


    /*!
     * \brief setFileRegisterBlockNumber
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP46nnn  OK
     *
     * nnn = 000 to 255
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setFileRegisterBlockNumber(ReadingMode mode);

    /*!
     * \brief getFileRegisterBlockNumber
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP46     46nnn
     *
     * nnn = 000 to 255
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   ReadingModeReturn getFileRegisterBlockNumber();

    /*!
     * \brief setPartitionMarkWhenAdditionalInformationIsUsed.
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP50hh  OK
     *
     * hh = Partition mark (1 character)
     * * Specify the mark in HEX (hexadecimal) code.
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   KeyenceError setPartitionMarkWhenAdditionalInformationIsUsed(ReadingMode mode);

    /*!
     * \brief getPartitionMarkWhenAdditionalInformationIsUsed.
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP50    50hh
     *
     * hh = Partition mark (1 character)
     * * Specify the mark in HEX (hexadecimal) code.
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   ReadingModeReturn getPartitionMarkWhenAdditionalInformationIsUsed();


    /*!
     * \brief setIntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed.
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP54hhh...  OK
     *
     * hhh = Intermediate delimiter (Up to 5 characters)
     * * Specify the mark in HEX (hexadecimal) code.
     * * To set no intermediate delimiter, hhh = FF.
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   KeyenceError setIntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed(ReadingMode mode);

    /*!
     * \brief getIntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed.
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP54    54hhh...
     *
     * hhh = Intermediate delimiter (Up to 5 characters)
     * * Specify the mark in HEX (hexadecimal) code.
     * * To set no intermediate delimiter, hhh = FF.
     *KeyenceError Codes: 00, 02, 05, 06, 10, 12, 14
     */
   ReadingModeReturn getIntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed();


    /*!
     * \brief setIndicationOfStabilityLED
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP09n  OK
     *
     * n=0: Disable
     *   1: Enable
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   KeyenceError setIndicationOfStabilityLED(ReadingMode mode);

    /*!
     * \brief getIndicationOfStabilityLED
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP09   09n
     *
     * n=0: Disable
     *   1: Enable
     *KeyenceError Codes: 00, 02, 05, 07, 14
     */
   ReadingModeReturn getIndicationOfStabilityLED();

    /*!
     * \brief setOK_NGOutputDuration
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP40nnn OK
     *
     * nnn= 001 to 255 (by 10 ms step)
     *
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setOK_NGOutputDuration(ReadingMode mode);

    /*!
     * \brief getOK_NGOutputDuration
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP40   40nnn
     *
     * nnn= 001 to 255 (by 10 ms step)
     *
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   ReadingModeReturn getOK_NGOutputDuration();


    /*!
     * \brief setRegistrationOfPresetDataForCompare
     * \param [in] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Change  WP68aaa...    OK
     *
     * aaa...= Preset data (Up to 32 characters)
     * * For CODE128, see P.136 to 137.
     * * To delete the preset data, send WP68.
     *
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
   KeyenceError setRegistrationOfPresetDataForCompare(ReadingMode mode);

    /*!
     * \brief setRegistrationOfPresetDataForCompare
     * \param [out] mode
     * \return TheKeyenceError code returned by the BL_700 device.
     *
     * Confirm RP68       68aaa...
     *
     * aaa...= Preset data (Up to 32 characters)
     * * For CODE128, see P.136 to 137.
     * * To delete the preset data, send WP68.
     *
     *KeyenceError Codes: 00, 02, 05, 09, 14
     */
    ReadingModeReturn getRegistrationOfPresetDataForCompare();

    StringReturn triggerOn();
    KeyenceError triggerOff();
    KeyenceError readingRateCheck();
    KeyenceError tactCheck();
    KeyenceError resetTest();
    KeyenceError turnOKOutputOn();
    KeyenceError turnNGOutputOn();
    KeyenceError turnOKNGOutputsOff();
    KeyenceError onlineTestOn();
    KeyenceError onlineTestOff();
    KeyenceError onlineTestCheck();
    KeyenceError clearTransmissionBuffer();
    KeyenceError enterSettingsMode();
    KeyenceError setToDefault();
    KeyenceError saveSettings();
    KeyenceError leaveSettingsMode();
    KeyenceError laserOff();
    KeyenceError resetLaserOff();
    KeyenceError resetSoftware();
    KeyenceError readoutHistoryCheck();

    void startContinuousRead();

    void stopContinuousRead();

public:

    explicit BL_700(QString info, QObject *parent = 0);
    ~BL_700();

    QString humanReadableKeyenceErrors(KeyenceError code);

    QString sendRawCommand(const QString & command, int timeout = -1);
    KeyenceError sendCommandAndParseResponse(const QString & command, QString & response, ModeOfOperation mode, int timeout = -1);
    KeyenceError sendNormalCommand(const QString & command, QString & response);
    KeyenceError sendSettingsCommand(const QString & command, QString &response);

    KeyenceError sendNormalCommand(const QString & command);
    KeyenceError sendSettingsCommand(const QString & command);



    KeyenceError codeBasedCommand(Preset code, KeyenceParameterCodes cmdCode, int parameter, int numDigits);

    KeyenceError codeBasedRetrieve(Preset code, KeyenceParameterCodes cmdCode, QString & response);

    KeyenceError writeParameter(KeyenceParameterCodes cmdCode, int parameter, int numDigits);

    KeyenceError retrieveParameter(KeyenceParameterCodes cmdCode, QString & response);

    DeviceStopBits convertToDeviceStopBits(const QString & stopbits);
    DeviceDataBits convertToDeviceDataBits(const QString & databits);
    DeviceParity   convertToDeviceParity(const QString & parity);
    DeviceBaudRate convertToDeviceBaudRate(const QString & baudrate);
    void setDeviceParams(const DeviceBaudRate baudrate,
                                 const DeviceParity   parity,
                                 const DeviceDataBits databits,
                                 const DeviceStopBits stopbits);
    static void registerMetaType();

signals:
    void messageRead(QString);
public slots:

private:

    QString commandPrefix();
    QString commandSuffix();
    QString mutateCommand(const QString & command);

    QMutex          command_mutex;
    ModeOfOperation settings_mode;
    QScopedPointer<QSerialPort> port;

    QObject * msgReader;
};

#endif // BL_700_H


