#ifndef RESPONSEREADER_H
#define RESPONSEREADER_H

#include <QIODevice>

/*!
 * \brief The ResponseReader class
 * A private implementation detail of the bl series barcodereader library.
 * Assists in reading one and only one message from the barcode reader.
 */
class MessageReader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString message READ getMessage)
public:

    /*!
     * \brief MessageReader
     * \param parent The QIODevice representing the barcode reader. A QtSerialPort, generally.
     */
    MessageReader(QIODevice * parent)
        : QObject(parent)
        , m_message()
    { }

    /*!
     * \brief getResponse
     * \return the response read from the barcode reader.
     */
    QString getMessage() const { return this->m_message; }

public slots:

    /*!
     * \brief readOneResponse
     * Commands the responsereader to synchronously
     * read one response from the barcode reader.
     * This will temporarily block your event loop.
     */
    void readOneMessage()
    {
        QIODevice * parent = qobject_cast<QIODevice *>(this->parent());
        Q_ASSERT(parent);
        {
            char c;
            while(parent->getChar(&c))
            {
                if('\r' != c)
                {
                    this->m_message.append(c);
                }
                else
                {
                    disconnect(this, SLOT(readOneMessage()));
                    emit messageRead(this->m_message);
                    return;
                }
            }
        }
    }

    /*!
     * \brief readContinuousMessage
     * Commands the responsereader to synchronously read messages from the
     * barcode reader until the readContinuousMessage is disconnected.
     * This will temporarily block your event loop.
     */
    void readContinuousMessage()
    {
        QIODevice * parent = qobject_cast<QIODevice *>(this->parent());
        Q_ASSERT(parent);
        {
            char c;
            while(parent->getChar(&c))
            {
                if('\r' != c)
                {
                    this->m_message.append(c);
                }
                else
                {
                    emit messageRead(this->m_message);
                    this->m_message.clear();
                }
            }
        }
    }
    /*!
     * \brief timeout
     * Disconnects the reader from any the readOneResponse slot and sets the response string to "timeout"
     */
    void timeout()
    {
        disconnect(this, SLOT(readOneMessage()));
        this->m_message = QStringLiteral("timeout");
    }

signals:

    /*!
     * \brief responseRead
     * Emitted when one response is read from the barcode reader.
     */
    void messageRead(QString);

private:
    QString m_message;
};
#endif // RESPONSEREADER_H
