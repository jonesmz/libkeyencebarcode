/*
 * This file is part of libkeyencebarcode.
 *
 * libkeyencebarcode is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkeyencebarcode is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkeyencebarcode. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2013 IntechDirect, Inc.
 */

#include "bl_700.h"

#include <QString>
#include <QEventLoop>
#include <QMutexLocker>

#include "messagereader.h"
#include <QMetaEnum>
#include <QTimer>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>

BL_700::BL_700(QString info, QObject *parent) :
    QObject(parent)
  , command_mutex()
  , settings_mode(NormalMode)
  , port(new QSerialPort(info))
  , msgReader(nullptr)
{
    qDebug() << "Opening " + this->port->portName();
    {
        QSerialPortInfo serialPortInfo(this->port->portName());

        qDebug() << endl
            << QObject::tr("Port: ") << serialPortInfo.portName() << endl
            << QObject::tr("Location: ") << serialPortInfo.systemLocation() << endl
            << QObject::tr("Description: ") << serialPortInfo.description() << endl
            << QObject::tr("Manufacturer: ") << serialPortInfo.manufacturer() << endl
            << QObject::tr("Vendor Identifier: ") << (serialPortInfo.hasVendorIdentifier() ? QByteArray::number(serialPortInfo.vendorIdentifier(), 16) : QByteArray()) << endl
            << QObject::tr("Product Identifier: ") << (serialPortInfo.hasProductIdentifier() ? QByteArray::number(serialPortInfo.productIdentifier(), 16) : QByteArray()) << endl
            << QObject::tr("Busy: ") << (serialPortInfo.isBusy() ? QObject::tr("Yes") : QObject::tr("No")) << endl;
    }

    if(! port->open(QIODevice::ReadWrite))
    {
        qFatal("Something horrible happened when opening the port->");
    }

    //Try out the default settings...
    port->setBaudRate(QSerialPort::Baud9600);
    port->setDataBits(QSerialPort::Data7);
    port->setParity(QSerialPort::EvenParity);
    port->setStopBits(QSerialPort::OneStop);
    {
        QString response;
        if(Timeout != this->sendCommandAndParseResponse("SSET", response, NormalMode, 1000))
        {
            this->settings_mode = SettingsMode;
            this->leaveSettingsMode();
        }
        else
        {
            qFatal("Can't talk to the barcodereader... quitting");
        }
    }
}


BL_700::~BL_700()
{

}

DeviceStopBits BL_700::convertToDeviceStopBits(const QString & stopbits)
{
    if(QStringLiteral("OneStop") == stopbits)
    {
        return OneBit;
    }
    else if(QStringLiteral("TwoStop") == stopbits)
    {
        return TwoBits;
    }
    else
    {
        qFatal("Unsupported parity detected. Something horrible happened.");
    }
}

DeviceDataBits BL_700::convertToDeviceDataBits(const QString & databits)
{
    if(QStringLiteral("Data7") == databits)
    {
        return SevenBits;
    }
    else if(QStringLiteral("Data8") == databits)
    {
        return EightBits;
    }
    else
    {
        qFatal("Unsupported databits detected. Something horrible happened.");
    }
}

DeviceParity BL_700::convertToDeviceParity(const QString & parity)
{
    if(QStringLiteral("EvenParity") == parity)
    {
        return device_even;
    }
    else if(QStringLiteral("OddParity") == parity)
    {
        return device_odd;
    }
    else
    {
        qFatal("Unsupported parity detected. Something horrible happened.");
    }
}

DeviceBaudRate BL_700::convertToDeviceBaudRate(const QString & baudrate)
{
    if(QStringLiteral("Baud1200") == baudrate)
    {
        return bps_1200;
    }
    else if(QStringLiteral("Baud2400") == baudrate)
    {
        return bps_2400;
    }
    else if(QStringLiteral("Baud4800") == baudrate)
    {
        return bps_4800;
    }
    else if(QStringLiteral("Baud9600") == baudrate)
    {
        return bps_9600;
    }
    else if(QStringLiteral("Baud19200") == baudrate)
    {
        return bps_19200;
    }
    else if(QStringLiteral("Baud38400") == baudrate)
    {
        return bps_38400;
    }
    else
    {
        qFatal("Unsupported baudrate detected. Something horrible happened.");
    }

}

void BL_700::setDeviceParams(const DeviceBaudRate baudrate,
                             const DeviceParity   parity,
                             const DeviceDataBits databits,
                             const DeviceStopBits stopbits)
{
    this->settings_mode = SettingsMode;

    //this->setDeviceBaudRate(baudrate);
    //this->setParityType(parity);
    //this->setDataBitLength(databits);
    //this->setStopBit(stopbits);
    //this->saveSettings();
    qDebug() << baudrate << parity << databits << stopbits << "\n";
    this->leaveSettingsMode();



}

QString BL_700::humanReadableKeyenceErrors(KeyenceError code)
{
    switch(code)
    {
        case UndefinedCommand:       return "Undefined command.";
        case CommandFormatIncorrect: return "Command format is incorrect.";
        case NothingCorresponds:     return "Nothing corresponds to the number in the command.";
        case WrongMValue:            return "\"m\" value (codes 1 to 4) is other than 0 to 3.";
        case BarcodeTypeNotSent:     return "\"Bar code type setting command\" was not sent first."
                    "Sending \"No. of readable digits setting command\" is invalid for UPC/EAN code.";
        case NumberTooLong:          return "The number in the command is too long.";
        case DataTooShort:           return "\"hhh...\" data is too short.";
        case WrongNValue:            return "\"n\" value is not 0 or 1.";
        case InvalidNValue:          return "\"n\" value is exceeding the setting range.";
        case InvalidNNNValue:        return "\"nnn\" or \"nn\" value is exceeding the setting range.";
        case NotInHex:               return "\"hhh...\" is not specified in HEX (hexadecimal) code.";
        case FFCannotBeSet:          return "\"hhh...=FF\" cannot be set.";
        case TooManyCharacters:      return "\"hhh...\" or \"aaa...\" contains more than the specified number of characters.";
        case InvalidCharacters:      return "Characters of \"aaa...\" are invalid.";
        case EEPROMCorrupt:          return "Data in the EEPROM may be damaged. Perform initial setup.";
        case InitialSettingsError:   return "KeyenceError in the area storing initial settings. Settings are automatically initialized.";
        case SlashNotFollowed:       return "\\ is not followed by !, ? or \\ in preset data.";
        case TwoExclaimationPoints:  return "Two !s exist in preset data.";
        case Timeout:                return "The command timed out";
        case ParseError:             return "KeyenceError parsing response";
        case WrongModeForFunction:   return "Called settings function in normal mode, or normal function in settings mode.";
        case NoError:                return "No KeyenceError detected.";
        case ContactKEYENCE:         return "The BL-700 may malfunction. Contact KEYENCE.";
        default:                     return "Unknown KeyenceError!";
    }
}

QString BL_700::commandPrefix()
{
    return QString("");
}
QString BL_700::commandSuffix()
{
    return QString("\r");
}

QString BL_700::mutateCommand(const QString & command)
{
    return this->commandPrefix()+command+this->commandSuffix();
}
void BL_700::startContinuousRead()
{
    if(nullptr == msgReader)
    {
        MessageReader * reader = new MessageReader(port.data());
        connect(reader, SIGNAL(messageRead(QString)), this, SIGNAL(messageRead(QString)));
        connect(port.data(), SIGNAL(readyRead()), reader, SLOT(readContinuousMessage()));
        msgReader = reader;
    }
}

void BL_700::stopContinuousRead()
{
    if(nullptr != msgReader)
    {
        delete msgReader;
        msgReader = nullptr;
    }
}

QString BL_700::sendRawCommand(const QString & command, int timeout)
{
    qDebug() << command;

    MessageReader reader(port.data());
    /*
     * A sub event loop allows us to block this function call without interrupting
     * the processing of signals for the rest of the program.
     */
    {
        QEventLoop loop;
        QObject::connect(&reader,     SIGNAL(messageRead(QString)), &loop,   SLOT(quit()));
        QObject::connect(port.data(), SIGNAL(readyRead()),          &reader, SLOT(readOneMessage()));
        /*
         * A shame that there's no realstic way to prevent allocating the qtimer
         * when no timeout is desired. Maybe instead of using -1 to indicate
         * infinite timeout, use function polymorphism instead?
         */
        {
            QTimer timer;
            QObject::connect(&timer, SIGNAL(timeout()), &reader, SLOT(timeout()));
            QObject::connect(&timer, SIGNAL(timeout()), &loop,   SLOT(quit()));

            /*
             * The mutexlocker ensures that only 1 command is being written
             * or read to/from the barcode reader at a time. When the
             * loop.exec() exits either because of timeout or because a
             * message is read off of the wire then the mutex will be
             * automatically unlocked by the mutexlocker falling out of scope.
             */
            {
                QMutexLocker command_locker(&command_mutex);
                port->write(this->mutateCommand(command).toLocal8Bit());
                if(timeout != -1)
                {
                    timer.start(timeout);
                }
                loop.exec();
            }
        }
    }
    return reader.getMessage();
}

KeyenceError BL_700::sendCommandAndParseResponse(const QString & command, QString & response, ModeOfOperation mode, int timeout)
{
    /*
     * Reset response code to some known default.
     */
    response.clear();
    if(this->settings_mode != mode)
    {
        return WrongModeForFunction;
    }
    else
    {
        QString cmdReply = sendRawCommand(command, timeout);
        if(cmdReply.startsWith(QStringLiteral("ERR")))
        {
            bool ok = false;
            uint retcode = cmdReply.remove(QStringLiteral("ERR")).toUInt(&ok);
            if(ok)
            {
                return static_cast<KeyenceError>(retcode);
            }
            else
            {
                return ParseError;
            }
        }
        if(cmdReply == QStringLiteral("timeout"))
        {
            return Timeout;
        }
        response = cmdReply;
        return NoError;
    }
}

KeyenceError BL_700::sendNormalCommand(const QString & command, QString & response)
{
    return sendCommandAndParseResponse(command, response, NormalMode);
}

KeyenceError BL_700::sendSettingsCommand(const QString & command, QString & response)
{
    return sendCommandAndParseResponse(command, response, SettingsMode);
}

KeyenceError BL_700::sendNormalCommand(const QString & command)
{
    QString response;
    return sendNormalCommand(command, response);
}

KeyenceError BL_700::sendSettingsCommand(const QString & command)
{
    QString response;
    return sendSettingsCommand(command, response);
}

StringReturn BL_700::triggerOn()
{
    QString response;
    KeyenceError err = this->sendNormalCommand("LON", response);
    return StringReturn(err, response);
}
KeyenceError BL_700::triggerOff()              { return this->sendNormalCommand   ("LOFF");   }
KeyenceError BL_700::readingRateCheck()        { return this->sendNormalCommand   ("TEST1");  }
KeyenceError BL_700::tactCheck()               { return this->sendNormalCommand   ("TEST2");  }
KeyenceError BL_700::resetTest()               { return this->sendNormalCommand   ("QUIT");   }
KeyenceError BL_700::turnOKOutputOn()          { return this->sendNormalCommand   ("OKON");   }
KeyenceError BL_700::turnNGOutputOn()          { return this->sendNormalCommand   ("NGON");   }
KeyenceError BL_700::turnOKNGOutputsOff()      { return this->sendNormalCommand   ("ALLOFF"); }
KeyenceError BL_700::onlineTestOn()            { return this->sendNormalCommand   ("#TEST1"); }
KeyenceError BL_700::onlineTestOff()           { return this->sendNormalCommand   ("#QUIT");  }
KeyenceError BL_700::onlineTestCheck()         { return this->sendNormalCommand   ("#TEST");  }
KeyenceError BL_700::clearTransmissionBuffer() { return this->sendNormalCommand   ("BCLR");   }
KeyenceError BL_700::enterSettingsMode()       { KeyenceError err = this->sendNormalCommand("SSET");
                                                  this->settings_mode = SettingsMode;
                                                  return err;                                  }
KeyenceError BL_700::saveSettings()            { return this->sendSettingsCommand ("SAVE");   }
KeyenceError BL_700::leaveSettingsMode()       { KeyenceError err = this->sendSettingsCommand ("SEND");
                                                  this->settings_mode = NormalMode;
                                                  return err;                                  }
KeyenceError BL_700::setToDefault()            { return this->sendSettingsCommand ("DFLT");   }
KeyenceError BL_700::laserOff()                { return this->sendNormalCommand   ("LOCK");   }
KeyenceError BL_700::resetLaserOff()           { return this->sendNormalCommand   ("UNLOCK"); }
KeyenceError BL_700::resetSoftware()           { return this->sendNormalCommand   ("RESET");  }
KeyenceError BL_700::readoutHistoryCheck()     { return this->sendNormalCommand   ("NUM");    }

KeyenceError BL_700::codeBasedCommand(Preset code, BL_700::KeyenceParameterCodes cmdCode, int parameter, int numDigits)
{
    return this->sendSettingsCommand(QStringLiteral("WC%1%2%3").arg(static_cast<int>(code))
                                     .arg(cmdCode, 2, 10, QLatin1Char('0'))
                                     .arg(parameter, numDigits, 10, QLatin1Char('0')));
}

KeyenceError BL_700::codeBasedRetrieve(Preset code, BL_700::KeyenceParameterCodes cmdCode, QString & response)
{
    return this->sendSettingsCommand(QStringLiteral("RC%1%2").arg(static_cast<int>(code))
                                     .arg(cmdCode, 2, 10, QLatin1Char('0')), response);
}

KeyenceError BL_700::writeParameter(BL_700::KeyenceParameterCodes cmdCode, int parameter, int numDigits)
{
    return this->sendSettingsCommand(QStringLiteral("WP%1%2").arg(cmdCode, 2, 10, QLatin1Char('0'))
                                     .arg(parameter, numDigits, 10, QLatin1Char('0')));
}

KeyenceError BL_700::retrieveParameter(BL_700::KeyenceParameterCodes cmdCode, QString & response)
{
    return this->sendSettingsCommand(QStringLiteral("RP%1").arg(cmdCode, 2, 10, QLatin1Char('0')), response);
}

KeyenceError BL_700::setBarCodeType(Preset code, BarcodeType type)
{

    return this->sendSettingsCommand(QStringLiteral("CODE%1=%2").arg(static_cast<int>(code))
                                     .arg(static_cast<int>(type)));
}
BarcodeTypeReturn BL_700::getBarCodeType(Preset code)
{
    QString response;
    this->sendSettingsCommand(QStringLiteral("CODE%1").arg(static_cast<int>(code)), response);
    return BarcodeTypeReturn();
}

KeyenceError BL_700::setMaxNumberOfReadableDigits(Preset code, uchar digits)
{
    return this->sendSettingsCommand(QStringLiteral("MAX%1=%2").arg(static_cast<int>(code))
                                     .arg(static_cast<int>(digits), 2, 10, QLatin1Char('0')));
}
UCharReturn BL_700::getMaxNumberOfReadableDigits(Preset code)
{
    QString response;
    this->sendSettingsCommand(QStringLiteral("MAX%1").arg(static_cast<int>(code)), response);
    return UCharReturn();
}

KeyenceError BL_700::setMinNumberOfReadableDigits(Preset code, uchar digits)
{
    return this->sendSettingsCommand(QStringLiteral("MIN%1=%2").arg(static_cast<int>(code))
                                     .arg(static_cast<int>(digits), 2, 10, QLatin1Char('0')));
}
UCharReturn BL_700::getMinNumberOfReadableDigits(Preset code)
{
    QString response;
    this->sendSettingsCommand(QStringLiteral("MIN%1").arg(static_cast<int>(code)), response);
    return UCharReturn();
}
KeyenceError BL_700::setCODE39SendingStartStopCharacter(Preset code, bool enable)
{
    return this->codeBasedCommand(code, BL_700::CODE39SendingStartStopCharacter, enable, 1);
}
BoolReturn BL_700::getCODE39SendingStartStopCharacter(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CODE39SendingStartStopCharacter, response);
    return BoolReturn();
}
KeyenceError BL_700::setCODE39InspectionOfCheckDigit(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::CODE39InspectionOfCheckDigit, setting, 1);
}
BoolReturn BL_700::getCODE39InspectionOfCheckDigit(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CODE39InspectionOfCheckDigit, response);
    return BoolReturn();
}
KeyenceError BL_700::setCODE39SendingCheckDigit(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::CODE39SendingCheckDigit, setting, 1);
}
BoolReturn BL_700::getCODE39SendingCheckDigit(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CODE39SendingCheckDigit, response);
    return BoolReturn();
}
KeyenceError BL_700::setITFInspectionOfCheckDigit(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::ITFInspectionOfCheckDigit, setting, 1);
}
BoolReturn BL_700::getITFInspectionOfCheckDigit(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::ITFInspectionOfCheckDigit, response);
    return BoolReturn();
}
KeyenceError BL_700::setITFSendingCheckDigit(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::ITFSendingCheckDigit, setting, 1);
}
BoolReturn BL_700::getITFSendingCheckDigit(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::ITFSendingCheckDigit, response);
    return BoolReturn();
}

KeyenceError BL_700::setCodabarSendingStartStopCharacter(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::CodabarSendingStartStopCharacter, setting, 1);
}
BoolReturn BL_700::getCodabarSendingStartStopCharacter(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CodabarSendingStartStopCharacter, response);
    return BoolReturn();
}
KeyenceError BL_700::setCodabarStopStartCharacterType(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::CodabarStopStartCharacterType, setting, 1);
}
BoolReturn BL_700::getCodabarStopStartCharacterType(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CodabarStopStartCharacterType, response);
    return BoolReturn();
}
KeyenceError BL_700::setCodabarInspectionOfCheckDigit(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::CodabarInspectionOfCheckDigit, setting, 1);
}
BoolReturn BL_700::getCodabarInspectionOfCheckDigit(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CodabarInspectionOfCheckDigit, response);
    return BoolReturn();
}
KeyenceError BL_700::setCodabarSendingCheckDigit(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::CodabarSendingCheckDigit, setting, 1);
}
BoolReturn BL_700::getCodabarSendingCheckDigit(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CodabarSendingCheckDigit, response);
    return BoolReturn();
}
KeyenceError BL_700::setCodabarSettingCheckDigitType(Preset code, BarcodeType checktype)
{
    return this->codeBasedCommand(code, BL_700::CodabarSettingCheckDigitType, checktype, 1);
}
BarcodeTypeReturn BL_700::getCodabarSettingCheckDigitType(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CodabarSettingCheckDigitType, response);
    return BarcodeTypeReturn();
}
KeyenceError BL_700::setEANUPCReadingUPC_E(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::EANUPCReadingUPC_E, setting, 1);
}
BoolReturn BL_700::getEANUPCReadingUPC_E(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::EANUPCReadingUPC_E, response);
    return BoolReturn();
}
KeyenceError BL_700::setEANUPCReadingEAN8Digits(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::EANUPCReadingEAN8Digits, setting, 1);
}

BoolReturn BL_700::getEANUPCReadingEAN8Digits(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::EANUPCReadingEAN8Digits, response);
    return BoolReturn();
}
KeyenceError BL_700::setEANUPCReadingJAN13Digits(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::EANUPCReadingJAN13Digits, setting, 1);
}
BoolReturn BL_700::getEANUPCReadingJAN13Digits(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::EANUPCReadingJAN13Digits, response);
    return BoolReturn();
}
KeyenceError BL_700::setEANUPCNoOfUPC_AOutputDigits(Preset code, OututDigits setting)
{
    return this->codeBasedCommand(code, BL_700::EANUPCNoOfUPC_AOutputDigits, setting, 1);
}
OututDigitsReturn BL_700::getEANUPCNoOfUPC_AOutputDigits(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::EANUPCNoOfUPC_AOutputDigits, response);
    return OututDigitsReturn();
}
KeyenceError BL_700::setEANAdding0ToUPC_ESystemCode(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::EANAdding0ToUPC_ESystemCode, setting, 1);
}
BoolReturn BL_700::getEANAdding0ToUPC_ESystemCode(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::EANAdding0ToUPC_ESystemCode, response);
    return BoolReturn();
}
KeyenceError BL_700::setCODE128CheckingDoubleCharacterStartPattern(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::CODE128CheckingDoubleCharacterStartPattern, setting, 1);
}
BoolReturn BL_700::getCODE128CheckingDoubleCharacterStartPattern(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::CODE128CheckingDoubleCharacterStartPattern, response);
    return BoolReturn();
}
KeyenceError BL_700::setMaxCodeLengthOutputFunction(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::MaxCodeLengthOutputFunction, setting, 1);
}
BoolReturn BL_700::getMaxCodeLengthOutputFunction(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::MaxCodeLengthOutputFunction, response);
    return BoolReturn();
}
KeyenceError BL_700::setDirectionForMaxCodeLengthOutput(Preset code, ForwardReverse setting)
{
    return this->codeBasedCommand(code, BL_700::DirectionForMaxCodeLengthOutput, setting, 1);
}
ForwardReverseReturn BL_700::getDirectionForMaxCodeLengthOutput(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::DirectionForMaxCodeLengthOutput, response);
    return ForwardReverseReturn();
}
KeyenceError BL_700::setEffectiveDigitsForMaxCodeLengthOutput(Preset code, CodeLengthDigits setting)
{
    return this->codeBasedCommand(code, BL_700::EffectiveDigitsForMaxCodeLengthOutput, setting, 1);
}
CodeLengthDigitsReturn BL_700::getEffectiveDigitsForMaxCodeLengthOutput(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::EffectiveDigitsForMaxCodeLengthOutput, response);
    return CodeLengthDigitsReturn();
}
KeyenceError BL_700::setStartDigitForMaxCodeLengthOutput(Preset code, CodeLengthDigits setting)
{
    return this->codeBasedCommand(code, BL_700::StartDigitForMaxCodeLengthOutput, setting, 1);
}
CodeLengthDigitsReturn BL_700::getStartDigitForMaxCodeLengthOutput(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::StartDigitForMaxCodeLengthOutput, response);
    return CodeLengthDigitsReturn();
}
KeyenceError BL_700::setLabelOrientationSpecifiedReading(Preset code, bool setting)
{
    return this->codeBasedCommand(code, BL_700::LabelOrientationSpecifiedReading, setting, 1);
}
BoolReturn BL_700::getLabelOrientationSpecifiedReading(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::LabelOrientationSpecifiedReading, response);
    return BoolReturn();
}
KeyenceError BL_700::setOrientationForOrientationSpecifiedReading(Preset code, ForwardReverse setting)
{
    return this->codeBasedCommand(code, BL_700::OrientationForOrientationSpecifiedReading, setting, 1);
}
ForwardReverseReturn BL_700::getOrientationForOrientationSpecifiedReading(Preset code)
{
    QString response;
    this->codeBasedRetrieve(code, BL_700::OrientationForOrientationSpecifiedReading, response);
    return ForwardReverseReturn();
}

KeyenceError BL_700::setReadingMode(ReadingMode mode)
{
    return this->writeParameter(BL_700::ReadingModeCmd, mode, 1);
}
ReadingModeReturn BL_700::getReadingMode()
{
    QString response;
    this->retrieveParameter(BL_700::ReadingModeCmd, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setDataSendTiming(ReadDataTiming mode)
{
    return this->writeParameter(BL_700::DataSendTiming, mode, 1);
}
ReadDataTimingReturn BL_700::getDataSendTiming()
{
    QString response;
    this->retrieveParameter(BL_700::DataSendTiming, response);
    return ReadDataTimingReturn();
}
KeyenceError BL_700::setRepeatReadTimeInMultiLabelReadingMode1Or2(ReadingMode mode)
{
    return this->writeParameter(BL_700::RepeatReadTimeInMultiLabelReadingMode1Or2, mode, 3);
}
ReadingModeReturn BL_700::getRepeatReadTimeInMultiLabelReadingMode1Or2()
{
    QString response;
    this->retrieveParameter(BL_700::RepeatReadTimeInMultiLabelReadingMode1Or2, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setSetDecodingMatchCount(ReadingMode mode)
{
    return this->writeParameter(BL_700::SetDecodingMatchCount, mode, 3);
}
ReadingModeReturn BL_700::getSetDecodingMatchCount()
{
    QString response;
    this->retrieveParameter(BL_700::SetDecodingMatchCount, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setDecodingMatchCountInAddnlInfo(ReadingMode mode)
{
    return this->writeParameter(BL_700::DecodingMatchCountInAddnlInfo, mode, 1);
}
ReadingModeReturn BL_700::getDecodingMatchCountInAddnlInfo()
{
    QString response;
    this->retrieveParameter(BL_700::DecodingMatchCountInAddnlInfo, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setScansInAddnlInfo(ReadingMode mode)
{
    return this->writeParameter(BL_700::ScansInAddnlInfo, mode, 1);
}
ReadingModeReturn BL_700::getScansInAddnlInfo()
{
    QString response;
    this->retrieveParameter(BL_700::ScansInAddnlInfo, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setLabelOrientationInAddnlInfo(ReadingMode mode)
{
    return this->writeParameter(BL_700::LabelOrientationInAddnlInfo, mode, 1);
}
ReadingModeReturn BL_700::getLabelOrientationInAddnlInfo()
{
    QString response;
    this->retrieveParameter(BL_700::LabelOrientationInAddnlInfo, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setCodeTypeInAddnlInfo(ReadingMode mode)
{
    return this->writeParameter(BL_700::CodeTypeInAddnlInfo, mode, 1);
}
ReadingModeReturn BL_700::getCodeTypeInAddnlInfo()
{
    QString response;
    this->retrieveParameter(BL_700::CodeTypeInAddnlInfo, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setReadingKeyenceErrorCode(ReadingMode mode)
{
    return this->writeParameter(BL_700::ReadingKeyenceErrorCode, mode, 1);
}
ReadingModeReturn BL_700::getReadingKeyenceErrorCode()
{
    QString response;
    this->retrieveParameter(BL_700::ReadingKeyenceErrorCode, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setSignalType(ReadingMode mode)
{
    return this->writeParameter(BL_700::SignalType, mode, 1);
}
ReadingModeReturn BL_700::getSignalType()
{
    QString response;
    this->retrieveParameter(BL_700::SignalType, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setOneShotInputTime(ReadingMode mode)
{
    return this->writeParameter(BL_700::OneShotInputTime, mode, 1);
}
ReadingModeReturn BL_700::getOneShotInputTime()
{
    QString response;
    this->retrieveParameter(BL_700::OneShotInputTime, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setTimeConstantOfTrigger(ReadingMode mode)
{
    return this->writeParameter(BL_700::TimeConstantOfTrigger, mode, 1);
}
ReadingModeReturn BL_700::getTimeConstantOfTrigger()
{
    QString response;
    this->retrieveParameter(BL_700::TimeConstantOfTrigger, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setStateOfTriggerInput(ReadingMode mode)
{
    return this->writeParameter(BL_700::StateOfTriggerInput, mode, 1);
}
ReadingModeReturn BL_700::getStateOfTriggerInput()
{
    QString response;
    this->retrieveParameter(BL_700::StateOfTriggerInput, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setStartTestModeWhenTriggerInputTurnsOn(ReadingMode mode)
{
    return this->writeParameter(BL_700::StartTestModeWhenTriggerInputTurnsOn, mode, 1);
}
ReadingModeReturn BL_700::getStartTestModeWhenTriggerInputTurnsOn()
{
    QString response;
    this->retrieveParameter(BL_700::StartTestModeWhenTriggerInputTurnsOn, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setTestModeToStartWhenTriggerInputTurnsOn(ReadingMode mode)
{
    return this->sendSettingsCommand(QStringLiteral("TRGT%1").arg(static_cast<int>(mode)));
}
ReadingModeReturn BL_700::getTestModeToStartWhenTriggerInputTurnsOn()
{
    QString response;
    this->sendSettingsCommand(QStringLiteral("TRGT"), response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setStartTestModeWhenPowerOn(ReadingMode mode)
{
    return this->sendSettingsCommand(QStringLiteral("TEST%1").arg(static_cast<int>(mode)));
}
ReadingModeReturn BL_700::getStartTestModeWhenPowerOn()
{
    QString response;
    this->sendSettingsCommand(QStringLiteral("TEST"), response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setCharactersOfTriggerOnCommand(ReadingMode mode)
{
    return this->writeParameter(BL_700::CharactersOfTriggerOnCommand, mode, 1);
}
ReadingModeReturn BL_700::getCharactersOfTriggerOnCommand()
{
    QString response;
    this->retrieveParameter(BL_700::CharactersOfTriggerOnCommand, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setCharactersOfTriggerOffCommand(ReadingMode mode)
{
    return this->writeParameter(BL_700::CharactersOfTriggerOffCommand, mode, 1);
}
ReadingModeReturn BL_700::getCharactersOfTriggerOffCommand()
{
    QString response;
    this->retrieveParameter(BL_700::CharactersOfTriggerOffCommand, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setDeviceBaudRate(DeviceBaudRate rate)
{
    return this->writeParameter(BL_700::DeviceBaudRateCmd, rate, 1);
}
DeviceBaudRateReturn BL_700::getDeviceBaudRate()
{
    QString response;
    this->retrieveParameter(BL_700::DeviceBaudRateCmd, response);
    return DeviceBaudRateReturn();
}
KeyenceError BL_700::setDataBitLength(DeviceDataBits mode)
{
    return this->writeParameter(BL_700::DataBitLength, mode, 1);
}
DeviceDataBitsReturn BL_700::getDataBitLength()
{
    QString response;
    this->retrieveParameter(BL_700::DataBitLength, response);
    return DeviceDataBitsReturn();
}
KeyenceError BL_700::setParityCheck(bool mode)
{
    return this->writeParameter(BL_700::ParityCheck, mode, 1);
}
BoolReturn BL_700::getParityCheck()
{
    QString response;
    this->retrieveParameter(BL_700::ParityCheck, response);
    return BoolReturn();
}
KeyenceError BL_700::setParityType(DeviceParity mode)
{
    return this->writeParameter(BL_700::ParityType, mode, 1);
}
DeviceParityReturn BL_700::getParityType()
{
    QString response;
    this->retrieveParameter(BL_700::ParityType, response);
    return DeviceParityReturn();
}
KeyenceError BL_700::setStopBit(DeviceStopBits mode)
{
    return this->writeParameter(BL_700::StopBit, mode, 1);
}
DeviceStopBitsReturn BL_700::getStopBit()
{
    QString response;
    this->retrieveParameter(BL_700::StopBit, response);
    return DeviceStopBitsReturn();
}
KeyenceError BL_700::setRTS_CTSHandshaking(ReadingMode mode)
{
    return this->writeParameter(BL_700::RTS_CTSHandshaking, mode, 1);
}
ReadingModeReturn BL_700::getRTS_CTSHandshaking()
{
    QString response;
    this->retrieveParameter(BL_700::RTS_CTSHandshaking, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setRS_485MultiDropLink(ReadingMode mode)
{
    return this->writeParameter(BL_700::RS_485MultiDropLink, mode, 1);
}
ReadingModeReturn BL_700::getRS_485MultiDropLink()
{
    QString response;
    this->retrieveParameter(BL_700::RS_485MultiDropLink, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setIDForRS_485MultiDropLink(ReadingMode mode)
{
    return this->writeParameter(BL_700::IDForRS_485MultiDropLink, mode, 2);
}
ReadingModeReturn BL_700::getIDForRS_485MultiDropLink()
{
    QString response;
    this->retrieveParameter(BL_700::IDForRS_485MultiDropLink, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setHandshakeProtocol1(ReadingMode mode)
{
    return this->writeParameter(BL_700::HandshakeProtocol1, mode, 1);
}
ReadingModeReturn BL_700::getHandshakeProtocol1()
{
    QString response;
    this->retrieveParameter(BL_700::HandshakeProtocol1, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setHandshakeProtocol2(ReadingMode mode)
{
    return this->writeParameter(BL_700::HandshakeProtocol2, mode, 1);
}
ReadingModeReturn BL_700::getHandshakeProtocol2()
{
    QString response;
    this->retrieveParameter(BL_700::HandshakeProtocol2, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setAddingChecksum(ReadingMode mode)
{
    return this->writeParameter(BL_700::AddingChecksum, mode, 1);
}
ReadingModeReturn BL_700::getAddingChecksum()
{
    QString response;
    this->retrieveParameter(BL_700::AddingChecksum, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setHeader(ReadingMode mode)
{
    return this->writeParameter(BL_700::Header, mode, 1);
}
ReadingModeReturn BL_700::getHeader()
{
    QString response;
    this->retrieveParameter(BL_700::Header, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setDelimiter(ReadingMode mode)
{
    return this->writeParameter(BL_700::Delimiter, mode, 1);
}
ReadingModeReturn BL_700::getDelimiter()
{
    QString response;
    this->retrieveParameter(BL_700::Delimiter, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setPLCLinkModel(ReadingMode mode)
{
    return this->writeParameter(BL_700::PLCLinkModel, mode, 1);
}
ReadingModeReturn BL_700::getPLCLinkModel()
{
    QString response;
    this->retrieveParameter(BL_700::PLCLinkModel, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setTriggerInputThroughPLCLink(ReadingMode mode)
{
    return this->writeParameter(BL_700::TriggerInputThroughPLCLink, mode, 1);
}
ReadingModeReturn BL_700::getTriggerInputThroughPLCLink()
{
    QString response;
    this->retrieveParameter(BL_700::TriggerInputThroughPLCLink, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setDMHeadAddress(ReadingMode mode)
{
    return this->writeParameter(BL_700::DMHeadAddress, mode, 1);
}
ReadingModeReturn BL_700::getDMHeadAddress()
{
    QString response;
    this->retrieveParameter(BL_700::DMHeadAddress, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setPLCStationNumber(ReadingMode mode)
{
    return this->writeParameter(BL_700::PLCStationNumber, mode, 1);
}
ReadingModeReturn BL_700::getPLCStationNumber()
{
    QString response;
    this->retrieveParameter(BL_700::PLCStationNumber, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setFileRegisterBlockNumber(ReadingMode mode)
{
    return this->writeParameter(BL_700::FileRegisterBlockNumber, mode, 1);
}
ReadingModeReturn BL_700::getFileRegisterBlockNumber()
{
    QString response;
    this->retrieveParameter(BL_700::FileRegisterBlockNumber, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setPartitionMarkWhenAdditionalInformationIsUsed(ReadingMode mode)
{
    return this->writeParameter(BL_700::PartitionMarkWhenAdditionalInformationIsUsed, mode, 1);
}
ReadingModeReturn BL_700::getPartitionMarkWhenAdditionalInformationIsUsed()
{
    QString response;
    this->retrieveParameter(BL_700::PartitionMarkWhenAdditionalInformationIsUsed, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setIntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed(ReadingMode mode)
{
    return this->writeParameter(BL_700::IntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed, mode, 1);
}
ReadingModeReturn BL_700::getIntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed()
{
    QString response;
    this->retrieveParameter(BL_700::IntermediateDelimiterWhenMultiLabelReadingMode2Or3IsUsed, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setIndicationOfStabilityLED(ReadingMode mode)
{
    return this->writeParameter(BL_700::IndicationOfStabilityLED, mode, 1);
}
ReadingModeReturn BL_700::getIndicationOfStabilityLED()
{
    QString response;
    this->retrieveParameter(BL_700::IndicationOfStabilityLED, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setOK_NGOutputDuration(ReadingMode mode)
{
    return this->writeParameter(BL_700::OK_NGOutputDuration, mode, 1);
}
ReadingModeReturn BL_700::getOK_NGOutputDuration()
{
    QString response;
    this->retrieveParameter(BL_700::OK_NGOutputDuration, response);
    return ReadingModeReturn();
}
KeyenceError BL_700::setRegistrationOfPresetDataForCompare(ReadingMode mode)
{
    return this->writeParameter(BL_700::RegistrationOfPresetDataForCompare, mode, 1);
}
ReadingModeReturn BL_700::getRegistrationOfPresetDataForCompare()
{
    QString response;
    this->retrieveParameter(BL_700::RegistrationOfPresetDataForCompare, response);
    return ReadingModeReturn();
}
