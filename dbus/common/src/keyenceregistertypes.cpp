#include "keyenceregistertypes.h"
#include "qdbusenummarshal.hpp"
#include "KeyenceTypes"

#include <QDBusMetaType>

void KeyenceRegisterTypes::registerMetaTypes()
{
    qRegisterMetaType<KeyenceError>("KeyenceError");
    qDBusRegisterMetaType<KeyenceError>();

    qRegisterMetaType<Preset>("Preset");
    qDBusRegisterMetaType<Preset>();

    qRegisterMetaType<DeviceBaudRate>("DeviceBaudRate");
    qDBusRegisterMetaType<DeviceBaudRate>();

    qRegisterMetaType<OututDigits>("OututDigits");
    qDBusRegisterMetaType<OututDigits>();

    qRegisterMetaType<BarcodeType>("BarcodeType");
    qDBusRegisterMetaType<BarcodeType>();

    qRegisterMetaType<ReadingMode>("ReadingMode");
    qDBusRegisterMetaType<ReadingMode>();

    qRegisterMetaType<ReadDataTiming>("ReadDataTiming");
    qDBusRegisterMetaType<ReadDataTiming>();

    qRegisterMetaType<ForwardReverse>("ForwardReverse");
    qDBusRegisterMetaType<ForwardReverse>();

    qRegisterMetaType<ModeOfOperation>("ModeOfOperation");
    qDBusRegisterMetaType<ModeOfOperation>();

    qRegisterMetaType<CodeLengthDigits>("CodeLengthDigits");
    qDBusRegisterMetaType<CodeLengthDigits>();

    qRegisterMetaType<BoolReturn>("BoolReturn");
    qDBusRegisterMetaType<BoolReturn>();

    qRegisterMetaType<UCharReturn>("UCharReturn");
    qDBusRegisterMetaType<UCharReturn>();

    qRegisterMetaType<StringReturn>("StringReturn");
    qDBusRegisterMetaType<StringReturn>();

    qRegisterMetaType<ReadingModeReturn>("ReadingModeReturn");
    qDBusRegisterMetaType<ReadingModeReturn>();

    qRegisterMetaType<BarcodeTypeReturn>("BarcodeTypeReturn");
    qDBusRegisterMetaType<BarcodeTypeReturn>();

    qRegisterMetaType<OututDigitsReturn>("OututDigitsReturn");
    qDBusRegisterMetaType<OututDigitsReturn>();

    qRegisterMetaType<DeviceParityReturn>("DeviceParityReturn");
    qDBusRegisterMetaType<DeviceParityReturn>();

    qRegisterMetaType<DeviceDataBitsReturn>("DeviceDataBitsReturn");
    qDBusRegisterMetaType<DeviceDataBitsReturn>();

    qRegisterMetaType<DeviceStopBitsReturn>("DeviceStopBitsReturn");
    qDBusRegisterMetaType<DeviceStopBitsReturn>();

    qRegisterMetaType<ReadDataTimingReturn>("ReadDataTimingReturn");
    qDBusRegisterMetaType<ReadDataTimingReturn>();

    qRegisterMetaType<ForwardReverseReturn>("ForwardReverseReturn");
    qDBusRegisterMetaType<ForwardReverseReturn>();

    qRegisterMetaType<DeviceBaudRateReturn>("DeviceBaudRateReturn");
    qDBusRegisterMetaType<DeviceBaudRateReturn>();

    qRegisterMetaType<CodeLengthDigitsReturn>("CodeLengthDigitsReturn");
    qDBusRegisterMetaType<CodeLengthDigitsReturn>();
}
