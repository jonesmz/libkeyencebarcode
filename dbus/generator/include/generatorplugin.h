#ifndef MYPLUGIN
#define MYPLUGIN
 
#include <QObject>
#include <QDBusMetaType>
 
class QDBusCpp2XmlPlugin
{
public:
    virtual ~QDBusCpp2XmlPlugin() {}
    virtual void registerMetaTypes() = 0;
};
Q_DECLARE_INTERFACE(QDBusCpp2XmlPlugin, "org.qt-project.Qt.DBus.Cpp2XmlPlugin")

class GeneratorPlugin : public QObject, public QDBusCpp2XmlPlugin
{
    Q_OBJECT
    Q_INTERFACES(QDBusCpp2XmlPlugin)
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.DBus.Cpp2XmlPlugin")
 
public:
    virtual void registerMetaTypes();
};
 
#endif //MYPLUGIN
