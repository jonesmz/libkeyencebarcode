include(../../project_settings.pri)

TARGET       = keyencedbusgeneratorplugin
TEMPLATE     = lib
QT          += dbus
CONFIG      += plugin

include(../common/common.pri)

SOURCES     += src/generatorplugin.cpp
HEADERS     += include/generatorplugin.h

QMAKE_POST_LINK += /home/m_jones/Downloads/qtbase-opensource-src-5.1.1/bin/qdbuscpp2xml -x $$PROJ_DIR/dist/libkeyencedbusgeneratorplugin.so $$PROJ_DIR/library/include/bl_700.h -o $$PROJ_DIR/dbus/library/include/dbusspec.xml ;
QMAKE_POST_LINK += /usr/lib/qt5/bin/qdbusxml2cpp $$PROJ_DIR/dbus/library/include/dbusspec.xml -a $$PROJ_DIR/dbus/library/include/adaptor -p $$PROJ_DIR/dbus/library/include/proxy -i KeyenceTypes;
QMAKE_POST_LINK += mv $$PROJ_DIR/dbus/library/include/*.cpp $$PROJ_DIR/dbus/library/src/ ;
