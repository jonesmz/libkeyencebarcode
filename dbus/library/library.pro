include(../../project_settings.pri)

DEPENDENCY_LIBRARIES = keyencebarcode
include(../../dependency.pri)

QT          += dbus

TARGET       = keyencedbus
TEMPLATE     = lib

SOURCES     += src/proxy.cpp src/adaptor.cpp
HEADERS     += include/proxy.h include/adaptor.h

include(../common/common.pri)
